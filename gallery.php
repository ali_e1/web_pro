<?php
session_start();
if (isset($_SESSION['user_id']))
{   
    $name=$_SESSION['name'];
    if (isset($_GET['message']))
    {
        if ($_GET['message'] == 'error') echo "error";
        else if ($_GET['message'] == 'empty') echo "empty";
    }
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GALLERY</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
    <style type="text/css">



label {
font-size: 150%;
}

</style>

</head>
<body>
    <nav class="navbar">
        <?php echo ' <a href="#" class="brand" style="font-size:250%;color:black;">'.$name.'</a>';?>
        <input id="close-menu" type="checkbox">
        <label for="close-menu" class="bars"></label>
        <ul class="navbar-menu">
            <li><a href="home.php">home</a></li>
            <li><a href="gallery.php"  class="active">gallery</a></li>
            <li><a href="landmarks.php">landmarks</a></li>
            <li><a href="blog.php">blog</a></li>
            <li><a href="reviews.php">reviews</a></li>
            <li><a href="logout.php">Log out</a></li>
        </ul>
    </nav>

    <section class="title-bar">
        <h3>Gallery</h3>
        <button id="addPhotoBtn">Add Photos</button>
    </section>

    <div class="gallery">
        <?php
        include ("connection.php");
        $stmt=$conn->prepare('select * from gallery');
        $stmt->execute();
        $gallery = $stmt->fetchAll();
        foreach($gallery as $g){
       echo '<div class="gallery-card">
            <div><img src="img/'.$g['image'].'" width="400" height="400"></div>
            <div class="img-desc">
                <p>'.$g['text'].'</p>
            </div>
        </div>';
    }
    ?>

    </div>

    <div class="add-modal" id="add-photo-modal">
        <div class="content">
            <h3>Add Photo</h3>
            <form action="add_gallery.php" method="post" enctype="multipart/form-data">
              
         &nbsp;&nbsp;&nbsp;<label>text</label><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<textarea name="gt" cols="60" rows="5"></textarea><br><br>
         <label>image</label><br><input type="file" name="gi"  placeholder="Image"/><br>
          <button type="submit" name="g_add">Add Site</button><br><br>
      </form>
        </div>
    </div>

    <footer>
        <p>2020 &copy Copyrights Reserved</p>
    </footer>

    <script src="js/main.js"></script>
</body>
</html>
<?php
}
else
{
    echo "error";
    header('Location:index.php');
}
?>