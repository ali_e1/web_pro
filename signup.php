 <!DOCTYPE html>

<html lang="en">
  <head>
  	<title>Sign up</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="css/style.css">

	</head>
	<body>
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				
			</div>
			<div class="row justify-content-center">
				<div class="col-md-7 col-lg-5">
					<div class="wrap">
						<div class="img" style="background-image: url(img/bg.jpg);"></div>
						<div class="login-wrap p-4 p-md-5">
			      	<div class="d-flex">
			      		<div class="w-100">
			      			<h3 class="mb-4">Sign In</h3>
			      		</div>
								<div class="w-100">
									<p class="social-media d-flex justify-content-end">
										<a href="#" class="social-icon d-flex align-items-center justify-content-center"><span class="fa fa-facebook"></span></a>
										<a href="#" class="social-icon d-flex align-items-center justify-content-center"><span class="fa fa-twitter"></span></a>
									</p>
								</div>
			      	</div>
							<form action="signin_insert.php" method="post" class="signin-form">
			      		<div class="form-group mt-3">
			      			<input name="uname" type="text" class="form-control" required>
			      			<label class="form-control-placeholder" for="username">Username</label>
			      		</div>
			      		<div class="form-group mt-3">
			      			<input name="email" type="text" class="form-control" required>
			      			<label class="form-control-placeholder" for="Email">Email</label>
			      		</div>
		            <div class="form-group">
		              <input name="pwd1" id="password-field" type="password" class="form-control" required>
		              <label class="form-control-placeholder" for="password">Password</label>
		              <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
		            </div>
		            <div class="form-group">
		              <input name="pwd2" id="password-field" type="password" class="form-control" required>
		              <label class="form-control-placeholder" for="password">Confirm Password</label>
		              <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
		            </div>
		            
			      		
			      		<div class="form-group mt-3">
			      			<input name="bday" type="date" class="form-control" required>
			      			<label class="form-control-placeholder" for="birthday">Birthday</label>
			      		</div>
			      		<div class="form-group mt-3">
			      			<input name="phone" type="number" class="form-control" required>
			      			<label class="form-control-placeholder" for="Phone">Phone</label>
			      		</div>
		            <div class="form-group">
		            	<button type="submit" class="form-control btn btn-primary rounded submit px-3">Sign up</button>
		            </div>
		            <div class="form-group d-md-flex">
		            	
									
		            </div>
		          </form>
		        </div>
		      </div>
				</div>
			</div>
		</div>
	</section>
	<div style="color:red">
            <?php
                if(isset($_GET['msg']) && !empty($_GET['msg'])){
                	if ($_GET['msg'] == 'error') echo "error";
        			else if ($_GET['msg'] == 'empty') echo "empty";
        			else if ($_GET['msg'] == 'sorry') echo '<script>alert("Sorry this email has been already exist")</script>'; 
                	
                }
            ?>
        </div>
	<script src="js/jquery.min.js"></script>
  <script src="js/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/main.js"></script>

	</body>
</html>

