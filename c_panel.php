<?php
session_start();
if (isset($_SESSION['user_id']))
{   
    $name=$_SESSION['name'];
    
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>lANDMARKS</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
</head>
<body>
    <nav class="navbar">
        <?php echo ' <a href="#" class="brand" style="font-size:250%;color:black;">'.$name.'</a>';?>
        <input id="close-menu" type="checkbox">
        <label for="close-menu" class="bars"></label>
        <ul class="navbar-menu">
            <li><a href="logout.php">Log out</a></li>
        </ul>
    </nav>
        <div class="title">
          <i class="fas fa-pencil-alt"></i> 
          <h2>Delete site</h2>
          <form action="delete.php" method="post">
              &nbsp;&nbsp;<label for="fname">choose site for edit</label>
             <?php
include ("connection.php");
$stmt1=$conn->prepare('select * from home');
$stmt1->execute();
$esite = $stmt1->fetchAll();
           echo    '<select  name="sid">';
                foreach($esite as $e){
            echo   '<option value="'.$e['id'].'">'.$e['sname'].'</option>';
            }
            ?>
            </select>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <button type="submit" name="delete_s">Delete site</button>
        </form>
        </div>

        <br>

    <form action="" method="post" enctype="multipart/form-data">
        <div class="title">
          <i class="fas fa-pencil-alt"></i> 
          <h2>Add new site</h2>
        </div>
        <div class="info">
            &nbsp;&nbsp; <label>Site_Name</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="sname" placeholder="site name">
          &nbsp;&nbsp; <label>text</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="site" placeholder="explain about site">
          &nbsp;&nbsp;<label>image</label>&nbsp;&nbsp;<input type="file" name="img1"  placeholder="Image"/>
          <button type="submit" name="s_add">Add Site</button><br><br>
        </div>

        <br><br>

         <div class="title">
          <i class="fas fa-pencil-alt"></i> 
          <h2>Delete Mark</h2>
          <form action="delete.php" method="post">
              &nbsp;&nbsp;<label for="fname">choose Pic</label>
             <?php
include ("connection.php");
$stmt1=$conn->prepare('select * from gallery');
$stmt1->execute();
$esite = $stmt1->fetchAll();
           echo    '<select  name="sid">';
                foreach($esite as $e){
            echo   '<option value="'.$e['id'].'">'.$e['id'].'</option>';
            }
            ?>
            </select>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <button type="submit" name="delete_g">Delete Photo</button>
        </form>
        </div>

        <br>

      </form>
          <form action="" method="post" enctype="multipart/form-data">
        <div class="title">
          <i class="fas fa-pencil-alt"></i> 
          <h2>Add new land marks</h2>
        </div>
        <div class="info">
            &nbsp;&nbsp; <label>Marks name</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="mname" placeholder="mark name">
          &nbsp;&nbsp; <label>Explain</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="mtext" placeholder="explain about site">
          &nbsp;&nbsp;<label>image</label>&nbsp;&nbsp;<input type="file" name="img2"  placeholder="Image"/>
          <button type="submit" name="add_m">Add Marks</button><br><br>
        </div>
      </form>
    <footer>
        <p>2020 &copy Copyrights Reserved</p>
    </footer>
<script src="js/main.js"></script>
</body>
</html>
<?php
include ("connection.php");
    
    
    try{
if(isset($_POST['s_add'])){
    $sname=$_POST['sname'];
    $st = $_POST['site'];
    $filename = $_FILES["img1"]["name"]; 
    $tempname = $_FILES["img1"]["tmp_name"];     
        $folder = "img/";  
        move_uploaded_file($tempname, $folder.$filename);
    $stmnt = $conn->prepare("Insert Into home(image,text,sname) values (:image,:text,:sname)");

    $stmnt ->bindparam(':image',$filename);
    $stmnt ->bindparam(':text',$st);
    $stmnt ->bindparam(':sname',$sname);

    $stmnt->execute();}


    if(isset($_POST['add_m'])){
    $mname=$_POST['mname'];
    $st = $_POST['mtext'];
    $filename = $_FILES["img2"]["name"]; 
    $tempname = $_FILES["img2"]["tmp_name"];     
        $folder = "img/";  
        move_uploaded_file($tempname, $folder.$filename);
    $stmnt = $conn->prepare("Insert Into marks(mname,mtext,image) values (:mname,:mtext,:image)");

    $stmnt ->bindparam(':image',$filename);
    $stmnt ->bindparam(':mtext',$st);
    $stmnt ->bindparam(':mname',$mname);

    $stmnt->execute();}



  }

    catch (PDOException $e) {
    echo "Error : ". $e->getMessage();
}



?>
<?php
}
else
{
    echo "error";
    header('Location:index.php');
}
?>