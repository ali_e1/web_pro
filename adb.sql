-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2021 at 08:18 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adb`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(3) NOT NULL,
  `image` varchar(40) NOT NULL,
  `pname` varchar(40) NOT NULL,
  `pdes` varchar(400) NOT NULL,
  `pdate` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `image`, `pname`, `pdes`, `pdate`) VALUES
(1, 'blog-01.jpg', 'Krumlov with kids', 'On holiday with your children? Český Krumlov offers loads of fun for you and your kids! While visiting, you’ll have plenty of options to choose from and literally won’t know where to go first! Stories from history, museum exhibitions suitable for children, interesting facts on local architecture, sports and cultural activities, all of this is on offer in Krumlov for your kids.', '2021-03-19'),
(2, 'blog-02.jpg', 'International Music Festival Český', 'The 29th International Music Festival Český Krumlov is a traditional event taking places this year from 18 September to 3 October. The new additions to the event schedule include a concert by the Pavel Černoch, a tenorist of worldwide renown, and a Czech Evening with performances by singers Marek Ztracený, Ondřej G. Brzobohatý and Ondřej Ruml.', '2019-07-21'),
(3, 'blog-03.jpg', 'Five-petalled Rose Celebrations', 'The largest cultural event in Český Krumlov, which is regulary held in the second half of June, is canceled due to coronavirus measures. The 34th year of the Five-Petalled Rose Celebrations doesn´t take place this year! This was decided by the City Council based on the advice of the municipal Security Council.', '2020-12-22');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(3) NOT NULL,
  `image` varchar(40) NOT NULL,
  `text` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `image`, `text`) VALUES
(3, 'gallery-01.jpg', '5th October 2006 - Wolfgang Amadeus Mozart - Requiem d moll, Closter Church'),
(4, 'gallery-02.jpg', 'Advent and Christmas in Český Krumlov\r\n                    1st December 2019 - 6th January 2020'),
(5, 'gallery-03.jpg', 'The grand opening of the Monasteries Český Krumlov 11th December 2015');

-- --------------------------------------------------------

--
-- Table structure for table `home`
--

CREATE TABLE `home` (
  `id` int(3) NOT NULL,
  `image` varchar(40) NOT NULL,
  `text` varchar(900) NOT NULL,
  `sname` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home`
--

INSERT INTO `home` (`id`, `image`, `text`, `sname`) VALUES
(3, 'history 11.jpg', 'The settlement arose beneath the castle, which was built from about 1240 onwards by a local branch of the noble Vítkovci family, descendants of Witiko of Prčice. The fortress was first mentioned in a 1253 deed as Chrumbenowe. It was also mentioned in', 'castle'),
(5, 'museums1.jpg', 'The Rosenbergs strongly promoted trade and crafts within the town walls. In the late 15th century, when gold was found next to the town, German miners came to settle, which shifted the ethnic balance even more. In one of the churches the sermons were preached in Czech until 1788, when St. Jošt Church was closed.[5] William of Rosenberg (1535–1592), High Treasurer and High Burgrave of Bohemia, had the castle rebuilt in a Renaissance style.', 'meusem');

-- --------------------------------------------------------

--
-- Table structure for table `marks`
--

CREATE TABLE `marks` (
  `id` int(3) NOT NULL,
  `mname` varchar(40) NOT NULL,
  `mtext` varchar(2000) NOT NULL,
  `image` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `marks`
--

INSERT INTO `marks` (`id`, `mname`, `mtext`, `image`) VALUES
(2, 'CASTLE', 'LČeský Krumlov Castle is unusually large for a town of its size; within the Czech Republic it is second in extent only to the Hradčany castle complex of Prague. The castle was first mentioned in written sources in 1240.[9] Inside its grounds are a large rococo garden, an extensive bridge over a deep gap in the rock upon which the castle is built, and the castle itself, which in turn consists of many defined parts dating from different periods. After the garden had been inadequately maintained in the second half of the 20th century, the site was included in the 1996 World Monuments Watch by the World Monuments Fund. With financial support from American Express the garden\'s central fountain was documented and reconstructed, and remains functional today.', 'castle.jpg'),
(3, 'MUSEUM', ' Český Krumlov has a museum dedicated to the painter Egon Schiele, who lived in the town. In the town center there is also a museum dedicated to the semi-precious gemstone Moldavite.<br>             Český Krumlov hosts a number of festivals and other events each year including the Five-Petaled Rose Festival (a reference to the rose of the Rožmberk crest), which is held on the summer solstice weekend. The downtown area is turned into a medieval town with craftsmen, artists, musicians, and local people in medieval costume. Activities include jousting, fencing, historical dance performances, and folk theater, in the Castle precincts and along the river. It concludes with a fireworks display.             The International Music Festival Český Krumlov begins in July and ends in August, and features international music of various genres.[15] Other such events are held throughout the year. The summer music festivals include the blues, rock, and soul festival Open Air Krumlov, held in late June at Eggenberg Brewery Garden.             Since the Velvet Revolution in 1989, over eighty restaurants have been established in the area. Many restaurants are located along the river and near the castle.', 'museums1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(3) NOT NULL,
  `image` varchar(40) NOT NULL,
  `rname` varchar(40) NOT NULL,
  `rdes` varchar(500) NOT NULL,
  `rdate` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `image`, `rname`, `rdes`, `rdate`) VALUES
(1, 'avatar.jpg', 'John Doe', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo totam amet expedita veniam dignissimos illum animi eos dolores, voluptatem quasi maiores possimus minus repudiandae unde ipsa blanditiis eaque reiciendis quibusdam dicta quas! Enim cum ab dolor itaque, vero repellendus distinctio.', '2021-03-18'),
(2, 'avatar.jpg', 'John Doe', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo totam amet expedita veniam dignissimos illum animi eos dolores, voluptatem quasi maiores possimus minus repudiandae unde ipsa blanditiis eaque reiciendis quibusdam dicta quas! Enim cum ab dolor itaque, vero repellendus distinctio.', '2021-04-01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pwd` varchar(40) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 0,
  `bday` date NOT NULL,
  `phone` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `pwd`, `type`, `bday`, `phone`) VALUES
(9, 'zozo', 'ss@gmail.com', '123', 0, '2020-12-16', 0),
(10, 'zozo', 'ss2@gmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 0, '2020-12-09', 0),
(11, 'zozo', 'ss3@gmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 0, '2020-12-02', 0),
(12, 'ali', 'ali@gmail.com', '123', 0, '2021-03-11', 0),
(13, 'test', 'test@gmail.com', '123', 0, '2004-11-13', 0),
(14, 'test1', 't@gmail.com', '123', 0, '2021-03-25', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `home`
--
ALTER TABLE `home`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `marks`
--
ALTER TABLE `marks`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
