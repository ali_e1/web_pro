<?php
session_start();
if (isset($_SESSION['user_id']))
{   
    $name=$_SESSION['name'];
    if (isset($_GET['message']))
    {
        if ($_GET['message'] == 'error') echo "error";
        else if ($_GET['message'] == 'empty') echo "empty";
    }
?>  
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HOME</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
</head>
<body>
    <header>
        <nav class="navbar">
           <?php echo ' <a href="#" class="brand" style="font-size:250%;color:black;">'.$name.'</a>';?>
            <input id="close-menu" type="checkbox">
            <label for="close-menu" class="bars"></label>
            <ul class="navbar-menu">
                <li><a href="home.php" class="active">home</a></li>
                <li><a href="gallery.php">gallery</a></li>
                <li><a href="landmarks.php">landmarks</a></li>
                <li><a href="blog.php">blog</a></li>
                <li><a href="reviews.php">reviews</a></li>
                <li><a href="logout.php">Log out</a></li>
                
            </ul>
        </nav>

        <div class="home-header">
            <div class="img-header"></div>
            <div class="header-title">
                <img src="" alt="">
                <h2>welcome to our town</h2>
                <p>Krumlov has its origin in Middle High German Krumme Aue, which can be translated
                    as crooked meadow, after a bend of the Vltava.</p>
            </div>
        </div>
    </header>


    <section id="history">
        <h3 class="section-title">History</h3>
        <?php
include ("connection.php");
$stmt=$conn->prepare('select * from home');
$stmt->execute();
$home = $stmt->fetchAll();
foreach($home as $h){
    echo '<p>sss</p>';
      echo '<div class="history-card">

            <img src="img/'.$h['image'].'" alt="">
            <p>'.$h['text'].'
            </p>
        </div>';
    }
    ?>


        
    </section>

    <section id="info">
        <h3 class="section-title">general information</h3>
        <div class="info-card">
            <div>
                <p><strong>Location</strong> Český Krumlov is situated in the southern part of the Czech Republic near the Austrian border (distance to Prague 180 km / 113 miles)
                The town is situated 492 metres (1610 ft.) above sea level in the Vltava river valley at the foothills of the Blanský Forest Nature Reserve.  
                48° 49´ latitude
                14° 20´ east longitude</p><hr>
                <p><strong>Climate</strong> Mild, humid
                    Altitude: 492 metres (1610 ft.)
                    Average annual temperature: 7.0 °C
                    Coldest months: January - average temperature: -2.9 °C
                    Warmest month: July - average temperature: 15.7 °C
                    Average yearly amount of rainfall: 644 mm </p><hr>
                <p><strong>Area</strong> 22.16 km2 (8.56 sq mi)</p><hr>
                <p><strong>Population</strong> Population (2020-01-01) 12,981</p>
            </div>
            <img src="img/info.JPG" alt="">
        </div>
        
    </section>


    <footer>
        <p>2020 &copy Copyrights Reserved</p>
    </footer>
</body>
</html>
<?php
}
else
{
    echo "error";
    header('Location:index.php');
}
?>