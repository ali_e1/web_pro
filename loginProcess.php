<?php
session_start();
   include ("connection.php");
   
    $user_email = trim($_POST['email']);
    $user_password = trim($_POST['password']);
    if (!empty($user_email) && !empty($user_password))
    {
       
        $query = "SELECT * FROM users WHERE email = ? AND pwd = ?";
        $stmt = $conn->prepare($query);
        $stmt->execute([$user_email, $user_password ]);
        $user = $stmt->fetch();

        $count = $stmt->rowCount();
        
        if ($count == 1)
        {       
            $_SESSION['user_id'] = $user['user_id'];
            $_SESSION['email'] = $user['email'];
            $_SESSION['name'] = $user['name'];
                  if ($user['user_id']==12) {
                 header('Location:c_panel.php?id='.$user['user_id'].'');
                 }
           else{
            $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/home.php';
            header('Location:home.php?id='.$user['user_id'].'');}
        }
        // else: count=0
        else
        {
            
            header('Location: ' . 'log.php?message=' . 'error');
        }
    }
   
    else
    {
        
        header('Location: ' . 'login.php?message=' . 'empty');
    }

?>
