<?php
if(isset($_POST['delete_s']))
{
	include('connection.php');
	$id=$_POST['sid'];
    $query = "DELETE FROM home WHERE id = ?";
	$stmt = $conn->prepare($query);
	if(!($stmt->execute([$id])))
    die('Error');
    else 
	else header('Location:c_panel.php');
}

if(isset($_POST['delete_g']))
{
	include('connection.php');
	$id=$_POST['sid'];
    $query = "DELETE FROM gallery WHERE id = ?";
	$stmt = $conn->prepare($query);
	if(!($stmt->execute([$id])))
    die('Error');
    else header('Location:c_panel.php');
	
}
?>