<?php
session_start();
if (isset($_SESSION['user_id']))
{   
    $name=$_SESSION['name'];
    if (isset($_GET['message']))
    {
        if ($_GET['message'] == 'error') echo "error";
        else if ($_GET['message'] == 'empty') echo "empty";
    }
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>lANDMARKS</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
</head>
<body>
    <nav class="navbar">
        <?php echo ' <a href="#" class="brand" style="font-size:250%;color:black;">'.$name.'</a>';?>
        <input id="close-menu" type="checkbox">
        <label for="close-menu" class="bars"></label>
        <ul class="navbar-menu">
            <li><a href="home.php">home</a></li>
            <li><a href="gallery.php">gallery</a></li>
            <li><a href="landmarks.php" class="active">landmarks</a></li>
            <li><a href="blog.php">blog</a></li>
            <li><a href="reviews.php">reviews</a></li>
            <li><a href="logout.php">Log out</a></li>
        </ul>
    </nav>
    <?php
include ("connection.php");
$stmt=$conn->prepare('select * from marks');
$stmt->execute();
$marks = $stmt->fetchAll();
foreach($marks as $m){
   echo ' <div class="landmarks-container">
        <h3>'.$m['mname'].'</h3>
        <div><img src="img/'.$m['image'].'" width="900" height="600">
            <br><br>
            <p>'.$m['mtext'].'</p>
            <div id="castle-img"></div>
        </div>
    </div>';

}
?>
    <footer>
        <p>2020 &copy Copyrights Reserved</p>
    </footer>

</body>
</html>
<?php
}
else
{
    echo "error";
    header('Location:index.php');
}
?>