<?php
session_start();
if (isset($_SESSION['user_id']))
{   
    $name=$_SESSION['name'];
    if (isset($_GET['message']))
    {
        if ($_GET['message'] == 'error') echo "error";
        else if ($_GET['message'] == 'empty') echo "empty";
    }
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BLOG</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
</head>
<body>
    <nav class="navbar">
        <?php echo ' <a href="#" class="brand" style="font-size:250%;color:black;">'.$name.'</a>';?>
        <input id="close-menu" type="checkbox">
        <label for="close-menu" class="bars"></label>
        <ul class="navbar-menu">
            <li><a href="home.php">home</a></li>
            <li><a href="gallery.php">gallery</a></li>
            <li><a href="landmarks.php">landmarks</a></li>
            <li><a href="blog.php" class="active">blog</a></li>
            <li><a href="reviews.php" >reviews</a></li>
            <li><a href="logout.php">Log out</a></li>
        </ul>
    </nav>

    <section class="title-bar">
        <h3>Latest Posts</h3>
        <button id="addPostBtn">Add Post</button>
    </section>

    <div id="blog-container">
        <?php
        include ("connection.php");
$stmt=$conn->prepare('select * from blog');
$stmt->execute();
$blog = $stmt->fetchAll();
foreach($blog as $b){
        echo '<div class="post-card">
            <div class="post-content">
                <h3>'.$b['pname'].'</h3>
                <p>'.$b['pdes'].'</p>
                <p class="post-date">'.$b['pdate'].'</p>
            </div>
            <img src="img/'.$b['image'].'">
        </div>';
    }
    ?>
     
    </div>
    
    <div class="add-modal" id="add-post-modal">
        <div class="content">
            <form action="add_post.php" method="post" enctype="multipart/form-data">
            <h3>Add Post</h3>
            <label>Post Title</label>
            <input type="text" name="pname">

            <label>Post Description</label>
            <br>
            <textarea  cols="60" rows="3" name="pdes"></textarea>
            <br>
            <input type="date" name="pdate">

            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" name="pimg" placeholder="choose pic">
            <button type="submit" name="pb">Add</button>
        </form>
        </div>
    </div>

    <footer>
        <p>2020 &copy Copyrights Reserved</p>
    </footer>

    <script src="js/main.js"></script>
</body>
</html>
<?php
}
else
{
    echo "error";
    header('Location:index.php');
}
?>