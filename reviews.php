<?php
session_start();
if (isset($_SESSION['user_id']))
{   
    $name=$_SESSION['name'];
    
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>REVIEWS</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
</head>
<body>
    <nav class="navbar">
        <?php echo ' <a href="#" class="brand" style="font-size:250%;color:black;">'.$name.'</a>';?>
        <input id="close-menu" type="checkbox">
        <label for="close-menu" class="bars"></label>
        <ul class="navbar-menu">
            <li><a href="home.php">home</a></li>
            <li><a href="gallery.php">gallery</a></li>
            <li><a href="landmarks.php">landmarks</a></li>
            <li><a href="blog.php">blog</a></li>
            <li><a href="reviews.php" class="active">reviews</a></li>
            <li><a href="logout.php">Log out</a></li>
        </ul>
    </nav>

    <section class="title-bar">
        <h3>Reviews</h3>
        <button id="addReviewBtn">Add Review</button>
    </section>

    <div id="reviews">
    <?php
        include ("connection.php");
$stmt=$conn->prepare('select * from review');
$stmt->execute();
$review = $stmt->fetchAll();
foreach($review as $r){
        echo'
        <div class="review-card">
            <img src="img/'.$r['image'].'" alt="">
            <div class="review-text">
                <h4>Review Title</h4>
                <p class="review-user">'.$r['rname'].'</p>
                <p class="review-desc">'.$r['rdes'].'</p>
                <p class="review-date">'.$r['rdate'].'</p>
            </div>
        </div>';

    }
    ?>

    </div>

    <div class="add-modal" id="add-review-modal">
        <div class="content">
            <form action="add_review.php" method="post" enctype="multipart/form-data">
            <h3>Add Reviews</h3>
            <label>Name</label>
            <input type="text" name="rname">

            <label>Review Description</label>
            <br>
            <textarea  cols="60" rows="3" name="rdes"></textarea>
            <br>
            <input type="date" name="rdate">

            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" name="rimg" placeholder="choose pic">
            <button type="submit" name="rb">Add</button>
        </form>
    </div>

    <footer>
        <p>2020 &copy Copyrights Reserved</p>
    </footer>

    <script src="js/main.js"></script>
</body>
</html>
<?php
}
else
{
    echo "error";
    header('Location:index.php');
}
?>